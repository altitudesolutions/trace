import { configTrace, ITraceConfig, Trace } from "../src";
import { clearFilters } from "../src/trace";
import { TraceChannel, TraceChannelFlags } from "../src/types";

import { TestOutput } from "./test-output";

/* tslint:disable:no-magic-numbers*/

export function applyEach(filter?: string | string[]): void {
    Trace(filter).audit("Ha", "a");
    Trace(filter).debug("Hd", "d");
    Trace(filter).error("He", "e");
    Trace(filter).write("Hs", "s");
}

export function applyEachExpectAll(
    output: TestOutput,
    filter?: string | string[]
): void {
    const startLength = output.length;
    applyEach(filter);
    evalExpected(
        output,
        startLength,
        resultsForChannels(TraceChannelFlags.All)
    );
}

export function applyEachExpectNone(
    output: TestOutput,
    filter?: string | string[]
): void {
    const startLength = output.length;
    applyEach(filter);
    output.expectLength(startLength);
}

export function applyEachExpectChannels(
    output: TestOutput,
    channels: TraceChannelFlags,
    filter?: string | string[]
): void {
    const startLength = output.length;
    applyEach(filter);
    evalExpected(output, startLength, resultsForChannels(channels));
}

export function configTest(config?: ITraceConfig): TestOutput {
    const output = new TestOutput();
    if (config == undefined) {
        config = {};
    }
    config.output = output;
    clearFilters();
    configTrace(config);

    return output;
}

function evalExpected(
    output: TestOutput,
    startLength: number,
    expected: string[]
): void {
    // output.expectLength(startLength + expect.length);
    for (let i = 0; i < expected.length; i += 1) {
        expect(output.events[startLength + i].message).toBe(expected[i]);
    }
}

function hasChannels(
    channels: TraceChannelFlags,
    mask: TraceChannelFlags
): boolean {
    // tslint:disable-next-line: no-bitwise
    return (channels & mask) === mask;
}

function resultsForChannels(channels: TraceChannelFlags): string[] {
    const r = [];

    if (hasChannels(channels, TraceChannelFlags.Audit)) {
        r.push(formatChannel("audit"));
    }

    if (hasChannels(channels, TraceChannelFlags.Debug)) {
        r.push(formatChannel("debug"));
    }

    if (hasChannels(channels, TraceChannelFlags.Error)) {
        r.push(formatChannel("error"));
    }

    if (hasChannels(channels, TraceChannelFlags.Std)) {
        r.push(formatChannel("std"));
    }

    return r;
}

function formatChannel(channel: TraceChannel): string {
    switch (channel) {
        case "audit":
            return "AUDIT*[Ha] a";

        case "debug":
            return "DEBUG*[Hd] d";

        case "error":
            return "ERROR*[He] e";

        case "std":
            return "TRACE*[Hs] s";

        default:
            throw new Error(`Invalid trace channel "${channel}"`);
    }
}
