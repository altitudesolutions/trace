import { configTrace, Trace } from "../src/";
import { TraceChannelFlags } from "../src/types";
import { TestOutput } from "./test-output";

describe("Trace Parameters", () => {
    it("Should pass parameters to output", () => {
        const output = new TestOutput();
        configTrace({ output: output, channels: TraceChannelFlags.All });
        Trace().write("h", "");
        evalParams(output, []);
        Trace().error("h", "");
        evalParams(output, []);
        Trace().debug("h", "");
        evalParams(output, []);
        Trace().audit("h", "");
        evalParams(output, []);

        Trace().write("h", "", 1, 2, 3);
        evalParams(output, [1, 2, 3]);
        Trace().error("h", "", 4, 5, 6);
        evalParams(output, [4, 5, 6]);
        Trace().debug("h", "", 7, 8, 9);
        evalParams(output, [7, 8, 9]);
        Trace().audit("h", "", 10, 11, 12);
        evalParams(output, [10, 11, 12]);
    });
});

function evalParams(output: TestOutput, p: any[]) {
    output.expectLength(1);
    expect(output.events[0].optionalParams).toEqual(p);
    output.clear();
}
