import { configTrace, setFilter } from "../src/";

import * as Helpers from "./helpers";
import { TestOutput } from "./test-output";

describe("Trace Disabled", () => {
    it("Should not output when disabled with filters enabled", () => {
        const output = new TestOutput();
        configTrace({ filtersEnabled: true });
        setFilter(["", "A", "B"]);
        Helpers.applyEachExpectNone(output);
        Helpers.applyEachExpectNone(output, "A");
        Helpers.applyEachExpectNone(output, "B");
        Helpers.applyEachExpectNone(output, ["A", "B"]);
    });

    it("Should not output when disabled with filters disabled", () => {
        const output = new TestOutput();
        configTrace({ filtersEnabled: false });
        Helpers.applyEachExpectNone(output);
    });
});
