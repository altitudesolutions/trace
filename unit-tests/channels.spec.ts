import { setChannels } from "../src/trace";
import { TraceChannelFlags } from "../src/types";

import * as Helpers from "./helpers";

describe("Trace Channels", () => {
    function test(channels?: TraceChannelFlags): void {
        const output = Helpers.configTest({ channels, filtersEnabled: false });
        const targets =
            channels != undefined ? channels : TraceChannelFlags.Error;
        Helpers.applyEachExpectChannels(output, targets);
    }

    function testSet(channels: TraceChannelFlags): void {
        const output = Helpers.configTest({
            channels: TraceChannelFlags.All,
            filtersEnabled: false
        });
        setChannels(channels);
        Helpers.applyEachExpectChannels(
            output,
            channels != undefined ? channels : TraceChannelFlags.All
        );
    }

    it("Should write only to the default channels", () => {
        test(TraceChannelFlags.Default);
        testSet(undefined as any);
    });

    it("Should write only to the specified channel", () => {
        test(TraceChannelFlags.Audit);
        test(TraceChannelFlags.Debug);
        test(TraceChannelFlags.Error);
        test(TraceChannelFlags.Std);

        testSet(TraceChannelFlags.Audit);
        testSet(TraceChannelFlags.Debug);
        testSet(TraceChannelFlags.Error);
        testSet(TraceChannelFlags.Std);
    });

    it("Should write to all channels", () => {
        test(TraceChannelFlags.All);
        testSet(TraceChannelFlags.All);
    });
});
