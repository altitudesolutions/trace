import { setFilter, unsetFilter } from "../src/";
import { clearFilters } from "../src/trace";
import { TraceChannelFlags } from "../src/types";

import * as Helpers from "./helpers";
import { TestOutput } from "./test-output";

describe("Trace Filters", () => {
    function getOutput(filtersEnabled: boolean): TestOutput {
        return Helpers.configTest({
            filtersEnabled,
            channels: TraceChannelFlags.All
        });
    }
    it("Should ignore filters when filters disabled", () => {
        const output = getOutput(false);
        setFilter("A");
        Helpers.applyEachExpectAll(output, "A");
        Helpers.applyEachExpectAll(output, "Z");
        Helpers.applyEachExpectAll(output, ["X", "Y"]);
    });

    it("Should apply filters when filters enabled", () => {
        const output = getOutput(true);
        setFilter("A");
        Helpers.applyEachExpectNone(output);
        Helpers.applyEachExpectNone(output, "Z");
        Helpers.applyEachExpectNone(output, ["X", "Y"]);
        Helpers.applyEachExpectAll(output, "A");
        Helpers.applyEachExpectAll(output, ["Z", "A"]);
        Helpers.applyEachExpectAll(output, ["A", "Z"]);
    });

    it("Should apply default filter", () => {
        const output = getOutput(true);
        setFilter("");
        Helpers.applyEachExpectNone(output, "Z");
        Helpers.applyEachExpectNone(output, ["X", "Y"]);
        Helpers.applyEachExpectAll(output, "");
        // tslint:disable-next-line: no-null-keyword
        Helpers.applyEachExpectAll(output, <any>(<unknown>null));
        Helpers.applyEachExpectAll(output, <any>(<unknown>undefined));
        Helpers.applyEachExpectAll(output);
    });

    it("Should unset filter", () => {
        const output = getOutput(true);
        Helpers.applyEachExpectNone(output, "A");
        setFilter("A");
        Helpers.applyEachExpectAll(output, "A");
        unsetFilter("");
        Helpers.applyEachExpectAll(output, "A");
        unsetFilter("B");
        Helpers.applyEachExpectAll(output, "A");
        unsetFilter("A");
        output.clear();
        Helpers.applyEachExpectNone(output, "A");
    });

    it("Should clear filters", () => {
        const output = getOutput(true);
        setFilter(["A", "B"]);
        Helpers.applyEachExpectAll(output, "A");
        Helpers.applyEachExpectAll(output, "B");
        clearFilters();
        Helpers.applyEachExpectNone(output, "A");
        Helpers.applyEachExpectNone(output, "B");
    });

    it("Should apply when one of any filter contexts set", () => {
        const output = getOutput(true);
        setFilter(["A", "B"]);
        Helpers.applyEachExpectAll(output, ["Z", "A"]);
        Helpers.applyEachExpectAll(output, ["B", "Z"]);
        Helpers.applyEachExpectAll(output, ["A", "B"]);
    });
});
