import { ConsoleStyleTraceOutput, TraceChannel } from "../src";

export interface TraceOutputEvent {
    message: string;
    optionalParams: any[];
}

export class TestOutput extends ConsoleStyleTraceOutput {
    readonly events: TraceOutputEvent[] = [];
    clear(): void {
        this.events.splice(0);
    }

    expectLength(n: number): void {
        expect(this.length)
            .withContext("Unexpected number of output events")
            .toBe(n);
    }

    protected output(
        _channel: TraceChannel,
        message: string,
        ...optionalParams: any[]
    ): void {
        this.events.push({ message, optionalParams });
    }

    get length(): number {
        return this.events.length;
    }
}
