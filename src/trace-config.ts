import { ITraceOutput } from "./trace-output";
import { TraceChannelFlags } from "./types";
/**
 * The `Trace` configuration interface.
 */
export interface ITraceConfig {
    /**
     * Specifies the enabled trace channels (writes to channels that are not enabled are ignored).
     * The default is `TraceChannelFlags.Error`.
     */
    channels?: TraceChannelFlags;
    /**
     * Determines whether or not trace filters are enabled. The default is `false`.
     * When enabled, only trace logs with the specified filter will be written.
     */
    filtersEnabled?: boolean;
    /**
     * The `TraceOutput` to write trace events to. If not specified, then tracing is disabled.
     */
    output?: ITraceOutput;
}
