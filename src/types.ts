/**
 * Flags that can be used to specify which trace channels are enabled.
 * Writes to disabled channels are simply ignored.
 */
export enum TraceChannelFlags {
    None = 0x0,
    Error = 0x1,
    Std = 0x2,
    Debug = 0x4,
    Audit = 0x8,
    // tslint:disable-next-line: no-bitwise
    Default = Error | Std,
    // tslint:disable-next-line: no-bitwise
    All = Error | Std | Debug | Audit
}

/** The channel to write a trace event to. */
export type TraceChannel = "std" | "error" | "debug" | "audit";
/** Header information describing a trace event. */
export type TraceHeader = string | object | (string | object)[];
