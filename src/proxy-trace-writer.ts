import * as Functions from "./functions";
import { Trace } from "./trace";
import { ITraceWriter } from "./trace-writer";
import { TraceHeader } from "./types";
/**
 * A `TraceWriter` implementation which prepends a default header and applies an optional filter to every output.
 * Useful for when you want composite types to write to the trace context of their containing type.
 */
export class ProxyTraceWriter implements ITraceWriter {
    constructor(
        private readonly defaultHeader: TraceHeader,
        private readonly filter?: string | string[]
    ) {
        if (defaultHeader == undefined) this.defaultHeader = this;
    }
    audit(
        header: TraceHeader,
        message: string,
        ...optionalParams: any[]
    ): void {
        Trace(this.filter).audit(
            Functions.prependHeaderElement(header, this.defaultHeader),
            message,
            ...optionalParams
        );
    }
    debug(
        header: TraceHeader,
        message: string,
        ...optionalParams: any[]
    ): void {
        Trace(this.filter).debug(
            Functions.prependHeaderElement(header, this.defaultHeader),
            message,
            ...optionalParams
        );
    }
    error(
        header: TraceHeader,
        message: string,
        ...optionalParams: any[]
    ): void {
        Trace(this.filter).error(
            Functions.prependHeaderElement(header, this.defaultHeader),
            message,
            ...optionalParams
        );
    }
    write(
        header: TraceHeader,
        message: string,
        ...optionalParams: any[]
    ): void {
        Trace(this.filter).write(
            Functions.prependHeaderElement(header, this.defaultHeader),
            message,
            ...optionalParams
        );
    }
}
