import { TraceHeader } from "./types";
/**
 * Appends an item to a `TraceHeader`.
 * @param header The existing header.
 * @param item  The item to append.
 */
export function appendHeaderElement(
    header: TraceHeader,
    item: string | object
): TraceHeader {
    if (header == undefined) return item;
    if (!Array.isArray(header)) {
        header = [header, item];
    } else {
        header.push(item);
    }

    return header;
}
/**
 * Prepends an item to a `TraceHeader`.
 * @param header The existing header.
 * @param item  The item to prepend.
 */
export function prependHeaderElement(
    header: TraceHeader,
    item: string | object
): TraceHeader {
    if (header == undefined) return item;
    if (!Array.isArray(header)) {
        header = [item, header];
    } else {
        header.unshift(item);
    }

    return header;
}
/**
 * Formats a `TraceHeader` item in the console output style.
 * @param header The header to format.
 * @returns The formatted header as a `string`.
 */
export function formatHeaderElement(
    header: string | object
): string | undefined {
    if (header == undefined) return "[undefined trace source]";
    if (typeof header === "string") return `[${header}]`;
    if (typeof header === "function") return `[${header.name}]`;

    return header.constructor != undefined
        ? header.constructor.name
        : // tslint:disable-next-line: no-unbound-method
        typeof header.toString === "function"
        ? `[${header.toString()}]`
        : `[${typeof header}]`;
}
