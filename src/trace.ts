import { ITraceConfig } from "./trace-config";
import { ITraceOutput } from "./trace-output";
import { ITraceWriter } from "./trace-writer";
import { TraceChannel, TraceChannelFlags, TraceHeader } from "./types";

let _writer: ITraceWriter;
let _mockWriter: ITraceWriter;
let _output: ITraceOutput | undefined;
let _enabled = false;
let _channels = TraceChannelFlags.None;
let _filtersEnabled = false;
let _filterSet: Set<string> | undefined;
/**
 * Returns the active `TraceWriter` for the current environment`.
 * @param filter Optional. If specified, the TraceWriter will only output if the filter is set.
 */
export function Trace(filter?: string | string[]): ITraceWriter {
    return canWrite(filter) ? writer() : mockWriter();
}
/**
 * Applies the specified configuration data to the current environment.
 * @param config The configuration data to apply. If undefined, then the default will be applied.
 */
export function configTrace(config?: ITraceConfig): void {
    _output = undefined;
    _enabled = false;
    _channels = TraceChannelFlags.Error;
    _filtersEnabled = false;
    if (config == undefined) return;
    _output = config.output;
    _filtersEnabled = config.filtersEnabled === true;
    _enabled = _output != undefined;
    setChannels(
        config.channels != undefined ? config.channels : TraceChannelFlags.Error
    );
}
/**
 * Returns `true` if tracing is enabled in the current environment, otherwise returns `false`.
 */
export function traceEnabled(): boolean {
    return _enabled === true;
}
/**
 * Sets a trace filter in the current environment.
 * @param filter The filter to set.
 */
export function setFilter(filter: string | string[]): void {
    if (filter == undefined) return;
    const fs = filterSet();
    if (Array.isArray(filter)) {
        filter.forEach(f => {
            fs.add(f);
        });
    } else {
        fs.add(filter);
    }
}
/**
 * Unsets a trace filter in the current environment.
 * @param filter The filter to unset.
 */
export function unsetFilter(filter: string | string[]): void {
    if (filter == undefined) return;
    const fs = filterSet();
    if (Array.isArray(filter)) {
        filter.forEach(f => {
            // tslint:disable-next-line: no-dynamic-delete
            fs.delete(f);
        });
    } else {
        // tslint:disable-next-line: no-dynamic-delete
        fs.delete(filter);
    }
}
/**
 * Clears all trace filters in the current environment.
 */
export function clearFilters(): void {
    _filterSet = undefined;
}
/**
 * Sets the active trace channels. Writes to inactive channels will be ignored.
 * @param channels the channels to set
 */
export function setChannels(channels: TraceChannelFlags): void {
    if (channels == undefined || typeof channels !== "number") return;
    // tslint:disable-next-line: no-bitwise
    _channels = channels & TraceChannelFlags.All;
}
function canWrite(filter?: string | string[]): boolean {
    if (!_enabled) return false;
    if (!_filtersEnabled) return true;
    if (filter == undefined) filter = "";
    const fs = filterSet();
    if (typeof filter === "string") return fs.has(filter);
    let r = false;
    for (let i = 0; !r && i < filter.length; i += 1) {
        const f = filter[i];
        r = fs.has(f != undefined ? f : "");
    }

    return r;
}

function filterSet(): Set<string> {
    if (_filterSet == undefined) {
        _filterSet = new Set<string>();
    }

    return _filterSet;
}

function mockWriter(): ITraceWriter {
    if (_mockWriter == undefined) {
        _mockWriter = {
            audit: (_h: TraceHeader, _m: string, ..._p: any[]): void =>
                undefined,
            debug: (_h: TraceHeader, _m: string, ..._p: any[]): void =>
                undefined,
            error: (_h: TraceHeader, _m: string, ..._p: any[]): void =>
                undefined,
            write: (_h: TraceHeader, _m: string, ..._p: any[]): void =>
                undefined
        };
    }

    return _mockWriter;
}

function output(): ITraceOutput {
    return <any>(<unknown>_output);
}

function writer(): ITraceWriter {
    if (_writer == undefined) {
        function write(
            flag: TraceChannelFlags,
            channel: TraceChannel,
            header: TraceHeader,
            message: string,
            optionalParams: any[]
        ): void {
            // tslint:disable-next-line: no-bitwise
            if ((_channels & flag) === flag) {
                output().write(channel, header, message, ...optionalParams);
            }
        }
        _writer = {
            audit: (
                header: TraceHeader,
                message: string,
                ...optionalParams: any[]
            ) =>
                write(
                    TraceChannelFlags.Audit,
                    "audit",
                    header,
                    message,
                    optionalParams
                ),
            debug: (
                header: TraceHeader,
                message: string,
                ...optionalParams: any[]
            ) =>
                write(
                    TraceChannelFlags.Debug,
                    "debug",
                    header,
                    message,
                    optionalParams
                ),
            error: (
                header: TraceHeader,
                message: string,
                ...optionalParams: any[]
            ) =>
                write(
                    TraceChannelFlags.Error,
                    "error",
                    header,
                    message,
                    optionalParams
                ),
            write: (
                header: TraceHeader,
                message: string,
                ...optionalParams: any[]
            ) =>
                write(
                    TraceChannelFlags.Std,
                    "std",
                    header,
                    message,
                    optionalParams
                )
        };
    }

    return _writer;
}
