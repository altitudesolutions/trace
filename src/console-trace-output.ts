import * as Functions from "./functions";
import { ITraceOutput } from "./trace-output";
import { TraceChannel, TraceHeader } from "./types";
/***
 * A base class `TraceOutput` implementation for types which output in the console style.
 */
export abstract class ConsoleStyleTraceOutput implements ITraceOutput {
    write(
        channel: TraceChannel,
        header: TraceHeader,
        message: string,
        ...optionalParams: any[]
    ): void {
        if (Array.isArray(header)) {
            const c = header;
            header = "";
            c.forEach(h => {
                const s = Functions.formatHeaderElement(h);
                if (s != undefined) header += s;
            });
        } else {
            header = <any>(<unknown>Functions.formatHeaderElement(header));
        }

        if (header == undefined || header === "") {
            header = "[Trace Logger]";
        }

        let ch: string;
        switch (channel) {
            case "audit":
                ch = "AUDIT";
                break;
            case "debug":
                ch = "DEBUG";
                break;
            case "error":
                ch = "ERROR";
                break;
            default:
                ch = "TRACE";
        }
        this.output(channel, `${ch}*${header} ${message}`, ...optionalParams);
    }
    protected abstract output(message: string, ...optionalParams: any[]): void;
}

// tslint:disable: no-console

/***
 * A `TraceOutput` implementation which writes trace events to the console.
 */
// tslint:disable-next-line: max-classes-per-file
export class ConsoleTraceOutput extends ConsoleStyleTraceOutput {
    protected output(
        channel: TraceChannel,
        message: string,
        ...optionalParams: any[]
    ): void {
        switch (channel) {
            case "audit":
            case "std":
                console.log(message, ...optionalParams);
                break;

            case "debug":
                console.debug(message, ...optionalParams);
                break;

            case "error":
                console.error(message, ...optionalParams);
                break;

            default:
        }
    }
}
