import { TraceChannel, TraceHeader } from "./types";
/**
 * An interface which supports the writing of trace data to an output stream.
 */
export interface ITraceOutput {
    /** Writes the trace data to the underlying stream. */
    write(
        channel: TraceChannel,
        header: TraceHeader,
        message: string,
        ...optionalParams: any[]
    ): void;
}
