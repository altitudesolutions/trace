import { TraceHeader } from "./types";
/** An interface which supports the writing of trace events to specific trace channels. */
export interface ITraceWriter {
    /**
     * Writes a trace message to the `audit` channel.
     * @param header The trace header(s).
     * @param message The trace message.
     * @param optionalParams Optional parameters to write to the trace output.
     */
    audit(header: TraceHeader, message: string, ...optionalParams: any[]): void;
    /**
     * Writes a trace message to the `debug` channel.
     * @param header The trace header(s).
     * @param message The trace message.
     * @param optionalParams Optional parameters to write to the trace output.
     */
    debug(header: TraceHeader, message: string, ...optionalParams: any[]): void;
    /**
     * Writes a trace message to the `error` channel.
     * @param header The trace header(s).
     * @param message The trace message.
     * @param optionalParams Optional parameters to write to the trace output.
     */
    error(header: TraceHeader, message: string, ...optionalParams: any[]): void;
    /**
     * Writes a trace message to the `std` channel.
     * @param header The trace header(s).
     * @param message The trace message.
     * @param optionalParams Optional parameters to write to the trace output.
     */
    write(header: TraceHeader, message: string, ...optionalParams: any[]): void;
}
